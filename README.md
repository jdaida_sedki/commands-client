to test this project you should to follow the next steps : 

1- After running project and execute sql.setup, open the browser and test the follwing webservice.

2- localhost/rampup/api/{clientID}/commands : will return the different commands with their details made by the given client identifier

3- localhost/rampup/api/{clientID}/commands/{commandID} : will return the command with its details for the given identifier and client identifier

4- localhost/rampup/api/products/{productID} : will return the product for the given product identifier

5- to test BDD :  you can runner the java class under src/test/java in package com.dyamix.test

(NB: there are two classe RunnerCommandClient.java to test the service of commands and RunnnerProduct.java to test the service product)