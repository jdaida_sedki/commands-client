/**
 * 
 */
package com.dynamix.commands.repository;

import com.dynamix.commands.dao.CommandsEntityDao;
import com.dynamix.commands.model.Client;

/**
 * @author Jdaida SEDKI
 *
 */
public interface ClientRepository extends CommandsEntityDao<Client, Long> {

}
