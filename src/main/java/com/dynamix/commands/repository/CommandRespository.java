/**
 * 
 */
package com.dynamix.commands.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.dynamix.commands.dao.CommandsEntityDao;
import com.dynamix.commands.model.Command;

/**
 * @author "Jdaida SEDKI"
 *
 */
public interface CommandRespository extends CommandsEntityDao<Command, Long> {

	@Query("select c from Command c  where c.client.id= ?1")
	List<Command> findCommandByClient(Long idClient);

	@Query("select c from Command c  where c.client.id= ?1 AND c.id= ?2")
	Command findOneCommandByClient(Long idClient, Long idCommand);

}
