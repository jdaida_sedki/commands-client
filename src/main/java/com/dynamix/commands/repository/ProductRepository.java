/**
 * 
 */
package com.dynamix.commands.repository;

import com.dynamix.commands.dao.CommandsEntityDao;
import com.dynamix.commands.model.Product;

/**
 * @author "Jdaida SEDKI"
 *
 */
public interface ProductRepository extends CommandsEntityDao<Product, Long> {

}
