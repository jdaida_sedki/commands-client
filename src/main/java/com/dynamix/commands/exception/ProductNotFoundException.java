/**
 * 
 */
package com.dynamix.commands.exception;

/**
 * @author "Jdaida SEDKI"
 *
 */
public class ProductNotFoundException extends GenericException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2433382354684721906L;
	
	public static final String EXCEPTION_CODE = "Command_Cient_001";

	public ProductNotFoundException() {
		super(EXCEPTION_CODE);
	}

	public ProductNotFoundException(String detail) {
		super(EXCEPTION_CODE, detail);
	}

	public ProductNotFoundException(String detail, Object... param) {
		super(EXCEPTION_CODE, detail, param);
	}

	public ProductNotFoundException(Throwable cause) {
		super(cause, EXCEPTION_CODE);
	}

	public ProductNotFoundException(Throwable cause, String detail) {
		super(cause, EXCEPTION_CODE, detail);
	}
}
