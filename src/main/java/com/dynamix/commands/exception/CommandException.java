package com.dynamix.commands.exception;

/**
 * 
 * @author "Jdaida SEDKI"
 *
 */
public class CommandException  extends GenericException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2433382354684721906L;
	
	public static final String EXCEPTION_CODE = "Command_Cient_002";

	public CommandException() {
		super(EXCEPTION_CODE);
	}

	public CommandException(String detail) {
		super(EXCEPTION_CODE, detail);
	}

	public CommandException(String detail, Object... param) {
		super(EXCEPTION_CODE, detail, param);
	}

	public CommandException(Throwable cause) {
		super(cause, EXCEPTION_CODE);
	}

	public CommandException(Throwable cause, String detail) {
		super(cause, EXCEPTION_CODE, detail);
	}
}
