package com.dynamix.commands.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author "Jdaida SEDKI"
 *
 */
public class GenericException extends RuntimeException {


	/**
	 * 
	 */
	private static final long serialVersionUID = -4439525819115404496L;
	private String errorCode;
	private String errorNumber;
	private String detailsMsg;
	private List<Object> params;

	public GenericException() {
		super();
	}

	public GenericException(Throwable cause) {
		super(cause);
	}

	public GenericException(String errorCode) {
		this.errorCode = errorCode;
		this.detailsMsg = null;
	}

	public GenericException(Throwable cause, String errorCode) {
		super(cause);
		this.errorCode = errorCode;
		this.detailsMsg = null;
	}

	public GenericException(String errorCode, String detailsMsg) {
		this.errorCode = errorCode;
		this.detailsMsg = detailsMsg;
	}

	public GenericException(Throwable cause, String errorCode, String detailsMsg) {
		super(cause);
		this.errorCode = errorCode;
		this.detailsMsg = detailsMsg;
	}

	public GenericException(String errorNumber, String errorCode, String detailsMsg) {
		this.errorCode = errorCode;
		this.detailsMsg = detailsMsg;
		this.errorNumber = errorNumber;
	}

	public GenericException(String errorCode, String detailsMsg, Object... params) {
		this.errorCode = errorCode;
		this.detailsMsg = detailsMsg;
		this.params = new ArrayList<Object>();
		for (Object object : params) {
			this.params.add(object);
		}
	}

	public GenericException(Throwable cause, String errorCode, String detailsMsg, Object... params) {
		super(cause);
		this.errorCode = errorCode;
		this.detailsMsg = detailsMsg;
		this.params = new ArrayList<Object>();
		for (Object object : params) {
			this.params.add(object);
		}
	}

	/*
	 * getters and setters .
	 */
	public String getErrorCode() {
		return errorCode;
	}

	public String getDetailsMsg() {
		return detailsMsg;
	}

	public List<Object> getParams() {
		return params;
	}

	/**
	 * @return the errorNumber
	 */
	public String getErrorNumber() {
		return errorNumber;
	}

	/**
	 * @param errorNumber
	 *            the errorNumber to set
	 */
	public void setErrorNumber(String errorNumber) {
		this.errorNumber = errorNumber;
	}
}