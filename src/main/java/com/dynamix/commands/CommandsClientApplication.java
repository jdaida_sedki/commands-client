package com.dynamix.commands;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.context.web.WebAppConfiguration;

import com.dynamix.commands.config.AppConfig;

/**
 * 
 * @author "Jdaida SEDKI"
 *
 */
@SpringBootApplication
@EnableAutoConfiguration
@WebAppConfiguration
@IntegrationTest
public class CommandsClientApplication {
	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class, args);
	}
}
