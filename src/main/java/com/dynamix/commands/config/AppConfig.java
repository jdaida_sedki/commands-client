package com.dynamix.commands.config;

import javax.annotation.PostConstruct;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import com.dynamix.commands.endpoint.CommandEndPoint;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

/**
 * 
 * @author "Jdaida SEDKI"
 *
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan("com.dynamix.commands.**")
@Import(SwaggerConfig.class)
public class AppConfig {

	@Autowired
	private CommandEndPoint commandEndPoint;

	@PostConstruct
	public void dataSource() {
		new EmbeddedDatabaseBuilder().setName("example-app").setType(EmbeddedDatabaseType.H2).addScript("setup.sql")
				.build();
	}

	@Bean(destroyMethod = "shutdown")
	public SpringBus cxf() {
		return new SpringBus();
	}

	@Bean(destroyMethod = "destroy")
	@DependsOn("cxf")
	public Server jaxRsServer() {
		final JAXRSServerFactoryBean factory = new JAXRSServerFactoryBean();
		factory.setServiceBean(commandEndPoint);
		factory.setProvider(new JacksonJsonProvider());
		factory.setBus(cxf());
		factory.setAddress("/");
		return factory.create();
	}

	@Bean
	public ServletRegistrationBean cxfServlet() {
		final ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new CXFServlet(),
				"/rampup/*");
		servletRegistrationBean.setLoadOnStartup(1);
		return servletRegistrationBean;
	}
	
}
