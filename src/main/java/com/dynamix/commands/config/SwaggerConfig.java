/**
 * 
 */
package com.dynamix.commands.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.dynamix.commands.endpoint.CommandEndPoint;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Jdaida Sedki
 *
 */
@EnableSwagger2
@ComponentScan(basePackageClasses = CommandEndPoint.class)
@Configuration
public class SwaggerConfig {

	private static final String SWAGGER_API_VERSION = "1.0";
	private static final String LICENCE_TEXT = "Licence";
	private static final String title = "comman client Rest API";
	private static final String description = "RESTFUL API for commands client";

	@Bean
	public Docket comandsApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				  .apiInfo(apiInfo())
		          .select()                                  
		          .apis(RequestHandlerSelectors.any())              
		          .paths(PathSelectors.any())                          
		          .build();
	}


	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title(title).description(description).license(LICENCE_TEXT)
				.version(SWAGGER_API_VERSION).build();
	}

}
