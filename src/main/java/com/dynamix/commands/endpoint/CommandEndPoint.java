/**
 * 
 */
package com.dynamix.commands.endpoint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.jaxrs.JaxRsLinkBuilder;
import org.springframework.stereotype.Component;

import com.dynamix.commands.model.Command;
import com.dynamix.commands.model.Details;
import com.dynamix.commands.model.Product;
import com.dynamix.commands.service.CommandService;
import com.dynamix.commands.service.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author "Jdaida SEDKI"
 *
 */
@Path("/api")
@Api(value="/api",produces=MediaType.APPLICATION_JSON)
@Component
public class CommandEndPoint {

	@Autowired
	private CommandService commandService;

	@Autowired
	private ProductService productService;

	/**
	 * this method will return a list of commands by the given client identifier
	 * 
	 * @param clientId
	 * @return
	 */
	@GET
	@Path("/{clientID}/commands/")
	@ApiOperation("Get the commands list by the client identifier id")
	@ApiResponses(value={@ApiResponse(code=200,message="OK",response=Command.class)})
	@Produces({ MediaType.APPLICATION_JSON})
	@Valid
	public Collection<Resource<Command>> getCommandsByClient(
			@NotNull @Pattern(regexp = "\\d+") @PathParam("clientID") Long clientId) {
		Collection<Command> commands = commandService.findCommandsByClientId(clientId);
		List<Resource<Command>> resources = new ArrayList<>();
		commands.stream().forEach(command -> {
			command.getDetails().stream()
					.forEach(details -> resources.add(getCommandResource(clientId, command, details)));
		});
		return resources;
	}

	/**
	 * this method will return a details of product from the given client
	 * identifier
	 * 
	 * @param idProduct
	 * @return
	 */
	@GET
	@Path("/products/{productID}")
	@Produces({ MediaType.APPLICATION_JSON })
	@Valid
	@NotNull
	public Resource<Product> getProductById(@NotNull @Pattern(regexp = "\\d+") @PathParam("productID") Long idProduct) {
		Product product = productService.findProductById(idProduct);
		Resource<Product> resource = new Resource<>(product);
		resource.add(
				JaxRsLinkBuilder.linkTo(CommandEndPoint.class).slash("products").slash(product.getId()).withSelfRel());
		return resource;
	}

	/**
	 * this method will return a command with their details by the given client
	 * identifier and the command identifier
	 * 
	 * @param idClient
	 * @param idCommand
	 * @return
	 */
	@GET
	@Path("/{clientID}/commands/{commandID}")
	@Produces({ MediaType.APPLICATION_JSON })
	@Valid
	public Resource<Command> getCommandById(@NotNull @Pattern(regexp = "\\d+") @PathParam("clientID") Long idClient,
			@NotNull @Pattern(regexp = "\\d+") @PathParam("commandID") Long idCommand) {
		Command command = commandService.findCommandByCommandIdAndClientId(idClient, idCommand);
		Resource<Command> resource = new Resource<>(command);
		command.getDetails().stream()
		.forEach(details -> resource.add(JaxRsLinkBuilder.linkTo(CommandEndPoint.class).slash(idClient).slash("commands")
				.slash(command.getId()).slash("details").slash(details.getId()).withRel("details")));
		return resource;
	}

	/**
	 * 
	 * @param clientId
	 * @param command
	 * @param details
	 * @return
	 */
	private Resource<Command> getCommandResource(Long clientId, Command command, Details details) {
		Resource<Command> resource = new Resource<>(command);
		resource.add(JaxRsLinkBuilder.linkTo(CommandEndPoint.class).slash(clientId).slash("commands")
				.slash(command.getId()).slash("details").slash(details.getId()).withRel("details"));
		return resource;
	}

}
