/**
 * 
 */
package com.dynamix.commands.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.dynamix.commands.model.AbstractCommandsEntity;

/**
 * 
 * @author Jdaida SEDKI
 *
 */
@NoRepositoryBean
public interface CommandsEntityDao<T extends AbstractCommandsEntity, ID extends Serializable>
extends JpaRepository<T, ID> {

}

