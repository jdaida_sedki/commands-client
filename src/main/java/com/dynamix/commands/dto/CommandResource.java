/**
 * 
 */
package com.dynamix.commands.dto;

import java.util.List;

import org.springframework.hateoas.Link;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author fatma
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommandResource {
	
	private Long id;
	
	private String name;
	
	private List<DetailsResource> details;
	
	private ClientResource client;
	
	private int number;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the details
	 */
	public List<DetailsResource> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<DetailsResource> details) {
		this.details = details;
	}

	/**
	 * @return the client
	 */
	public ClientResource getClient() {
		return client;
	}

	/**
	 * @param client the client to set
	 */
	public void setClient(ClientResource client) {
		this.client = client;
	}

	/**
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(int number) {
		this.number = number;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

}
