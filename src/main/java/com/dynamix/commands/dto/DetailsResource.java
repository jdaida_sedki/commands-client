/**
 * 
 */
package com.dynamix.commands.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author fatma
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DetailsResource {
	
	private String description;
	
	private String price;
	
	private String supportedCommad;
	
	private String tax;
	
	private ProductResource productModel;

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the supportedCommad
	 */
	public String getSupportedCommad() {
		return supportedCommad;
	}

	/**
	 * @param supportedCommad the supportedCommad to set
	 */
	public void setSupportedCommad(String supportedCommad) {
		this.supportedCommad = supportedCommad;
	}

	/**
	 * @return the tax
	 */
	public String getTax() {
		return tax;
	}

	/**
	 * @param tax the tax to set
	 */
	public void setTax(String tax) {
		this.tax = tax;
	}

	/**
	 * @return the productModel
	 */
	public ProductResource getProductModel() {
		return productModel;
	}

	/**
	 * @param productModel the productModel to set
	 */
	public void setProductModel(ProductResource productModel) {
		this.productModel = productModel;
	}
	 

}
