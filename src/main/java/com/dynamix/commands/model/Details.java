/**
 * 
 */
package com.dynamix.commands.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

/**
 * @author Jdaida SEDKI
 *
 */
@Entity
public class Details extends AbstractCommandsEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6424674381816098944L;
	@NotNull @Size( min = 1, max = 255 ) 
	private String description;
	
	@NotNull
	private String price;
	
	@Null
	private String supportedCommad;
	
	@NotNull
	private String tax;
	
	@OneToOne
	private Product product;
	

	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the price
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(String price) {
		this.price = price;
	}

	/**
	 * @return the supportedCommad
	 */
	public String getSupportedCommad() {
		return supportedCommad;
	}

	/**
	 * @param supportedCommad the supportedCommad to set
	 */
	public void setSupportedCommad(String supportedCommad) {
		this.supportedCommad = supportedCommad;
	}

	/**
	 * @return the tax
	 */
	public String getTax() {
		return tax;
	}

	/**
	 * @param tax the tax to set
	 */
	public void setTax(String tax) {
		this.tax = tax;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

//	/**
//	 * @return the command
//	 */
//	public Command getCommand() {
//		return command;
//	}
//
//	/**
//	 * @param command the command to set
//	 */
//	public void setCommand(Command command) {
//		this.command = command;
//	}
	
}
