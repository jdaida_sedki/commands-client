/**
 * 
 */
package com.dynamix.commands.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

import org.springframework.hateoas.Link;

/**
 * @author Jdaida SEDKI
 *
 */
@Entity
public class Command extends AbstractCommandsEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -31268916383198279L;
	
	@NotNull @Size( min = 1, max = 255 ) 
	private String name;
	
	@Null
	private String number;

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    private List<Details> details;
	
	@ManyToOne
	private Client client;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the details
	 */
	public List<Details> getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(List<Details> details) {
		this.details = details;
	}

	/**
	 * @return the client
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * @param client the client to set
	 */
	public void setClient(Client client) {
		this.client = client;
	}


}
