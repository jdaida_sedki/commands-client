/**
 * 
 */
package com.dynamix.commands.service;

import com.dynamix.commands.model.Product;

/**
 * @author "Jdaida SEDKI"
 *
 */
@FunctionalInterface
public interface ProductService {
	
	/**
	 * find product by id product 
	 * @param idProduct
	 * @return
	 */
	Product findProductById(Long idProduct);

}
