/**
 * 
 */
package com.dynamix.commands.service;

import java.util.List;

import com.dynamix.commands.model.Command;

/**
 * @author "Jdaida SEDKI"
 *
 */
public interface CommandService {
	
	/**
	 * 
	 * @param idClient
	 * @return
	 */
	List<Command>findCommandsByClientId(Long idClient);
	
	/**
	 * 
	 * @param idClient
	 * @param idCommand
	 * @return
	 */
	Command findCommandByCommandIdAndClientId(Long idClient,Long idCommand);
	
	
	Command findCommandById(Long idCommand);

}
