/**
 * 
 */
package com.dynamix.commands.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dynamix.commands.exception.CommandException;
import com.dynamix.commands.model.Command;
import com.dynamix.commands.repository.CommandRespository;
import com.dynamix.commands.service.CommandService;

/**
 * @author "Jdaida SEDKI"
 *
 */
@Service
public class CommandServiceImpl implements CommandService{
	
	@Autowired
	private CommandRespository commandRespository;

	@Override
	public List<Command> findCommandsByClientId(Long idClient) {
		List<Command>commands= commandRespository.findCommandByClient(idClient);
		Optional<List<Command>> checkExistCommands = Optional.ofNullable(commands);
		if (checkExistCommands.isPresent()) {
			return commands;
		} else {
			throw new CommandException(
					"!!there is No Commands affected to client witch have the identifier : " + idClient);
		}
	}

	@Override
	public Command findCommandByCommandIdAndClientId(Long idClient, Long idCommand) {
		Command command = commandRespository.findOneCommandByClient(idClient, idCommand);
		Optional<Command> checkExistCommand = Optional.ofNullable(command);
		if (checkExistCommand.isPresent()) {
			return command;
		} else {
			throw new CommandException(
					"Command with the input command identifier and client identifier not Exist in DB!!");
		}
	}

	@Override
	public Command findCommandById(Long idCommand) {

		return commandRespository.findOne(idCommand);
	}
	
	

}
