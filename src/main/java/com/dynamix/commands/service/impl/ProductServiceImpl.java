/**
 * 
 */
package com.dynamix.commands.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dynamix.commands.exception.ProductNotFoundException;
import com.dynamix.commands.model.Product;
import com.dynamix.commands.repository.ProductRepository;
import com.dynamix.commands.service.ProductService;

/**
 * @author "Jdaida SEDKI"
 *
 */
@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductRepository productRepository;
	
	
	@Override
	public Product findProductById(Long idProduct) {
		Product product = productRepository.findOne(idProduct);
		Optional<Product> checkExistProduct = Optional.ofNullable(product);
		if (checkExistProduct.isPresent()) {
			return product;
		} else {
			throw new ProductNotFoundException("!!Product Not Found in DB with id product :"+idProduct);
		}
	}

	
}
