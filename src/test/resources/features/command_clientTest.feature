Feature: Command Test web service REST

  Scenario: get commands by the given client identifier
  	Given client have one or several commands in DB 
    When I search a commands of client by the identifier id "1"
    Then I get a list of "2" commands
    And this list of commmands contain the name attribute value
    |command02| 
    |command02|
    And Also contain the following number value 
    |10|
    |10|


  Scenario: get commands by the given client identifier and command identifier
  	Given client have at least one command in DB 
    When I find a command of client by the identifier id "1" and the command idendifier id "2"
    Then I get the Identifier Command containing the name attribute value "command04"
    And  number attribute value "11"
