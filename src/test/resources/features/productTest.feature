Feature: Product Test web service REST

  Scenario: get product by the given identifier
    Given the database have N products
    When I Recover the Product with id "1"
    Then I get the Identifier Product containing the code attribute value "product1"
    And  label attribute value "productOne"
