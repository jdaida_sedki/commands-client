/**
 * 
 */
package com.dynamix.test;

import java.util.Properties;

import javax.annotation.PostConstruct;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext;
import org.springframework.util.StringUtils;

import com.dynamix.commands.CommandsClientApplication;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

/**
 * @author Jdaida SEDKI
 *
 */
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(plugin = { "html:reports/taskManager/ReportFlowcucumber-html-report", "junit:reports/taskManager/ReportFlowcucumber-junit.xml",
		"json:reports/taskManager/ReportFlowcucumber.json", "pretty:reports/taskManager/ReportFlowcucumber-pretty.txt",
		"usage:reports/taskManager/ReportFlowcucumber-usage.json" }, features = {
				"src/test/resources/features/command_clientTest.feature" }, glue = {
						"com.dynamix.test.stepdefs" }, tags = { "~@Skip" })
public class RunnerCommandClient {
	
	@Autowired
	@Qualifier("dataProperties")
	private Properties dataProperties;

	@PostConstruct
	public void bootApplication() {
		EmbeddedWebApplicationContext webApplicationContext
				= (EmbeddedWebApplicationContext) SpringApplication.run( CommandsClientApplication.class );

		// retrieve the actual webserver port
		int webServerPort = webApplicationContext.getEmbeddedServletContainer().getPort();

		// modify the rest.base.url, replace the default port by the actual
		String restBaseUrl = dataProperties.get( "rest.base.url" ).toString();
		dataProperties.put( "rest.base.url", StringUtils.replace( restBaseUrl, "8080", "" + webServerPort ) );
	}

}
