/**
 * 
 */
package com.dynamix.test;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

/**
 * @author fatma
 *
 */
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(plugin = { "html:reports/taskManager/ReportFlowcucumber-html-report", "junit:reports/taskManager/ReportFlowcucumber-junit.xml",
		"json:reports/taskManager/ReportFlowcucumber.json", "pretty:reports/taskManager/ReportFlowcucumber-pretty.txt",
		"usage:reports/taskManager/ReportFlowcucumber-usage.json" }, features = {
				"src/test/resources/features/productTest.feature" }, glue = {
						"com.dynamix.test.stepdefs" }, tags = { "~@Skip" })

public class RunnnerProduct {

}
