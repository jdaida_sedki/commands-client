/**
 * 
 */
package com.dynamix.test.stepdefs;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;

import com.dynamix.commands.dto.CommandResource;
import com.dynamix.test.ws.RetrieveUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author Jdaida SEDKI
 *
 */

public class CommandClientFeatureDef {
    
	/**
	 * variable for httpResponse to recuperate the result of service web response
	 */
	HttpResponse httpResponse = null;
	
	/**
	 * object variable of command
	 */
	CommandResource command;
    
	/**
	 * list of commands 
	 */
	List<CommandResource> commands;
	
	
	
	
	@Given("^client have one or several commands in DB$")
	public void client_have_one_ot_several_commands_in_DB() throws Throwable {
	}

	@When("^I search a commands of client by the identifier id \"(.*?)\"$")
	public void i_search_a_commands_of_client_by_the_identifier_id(int arg1) throws Throwable {
		HttpUriRequest request = new HttpGet("http://localhost:8080/rampup/api/"+arg1+"/commands");
		httpResponse = HttpClientBuilder.create().build().execute(request);
		HttpEntity entity = httpResponse.getEntity();
		String result = EntityUtils.toString(entity);
		ObjectMapper mapper = new ObjectMapper();
		commands = mapper.readValue(result, new TypeReference<List<CommandResource>>() {
		});
	}

	@Then("^I get a list of \"(.*?)\" commands$")
	public void i_get_a_list_of_commands(String arg1) throws Throwable {
		Assert.assertEquals(commands.size(), Integer.parseInt(arg1));
	}

	@Then("^this list of commmands contain the name attribute value$")
	public void this_list_of_commmands_contain_the_name_attribute_value(List<String> arg1) throws Throwable {
		List<String> commandNames = commands.stream().map(CommandResource::getName).collect(Collectors.toList());
		commandNames.forEach(System.out::println);
		Assert.assertEquals(arg1,commandNames);
	}

	@Then("^Also contain the following number value$")
	public void also_contain_the_following_number_value(List<Integer> arg1) throws Throwable {
		List<Integer> commandsNumber = commands.stream().map(CommandResource::getNumber).collect(Collectors.toList());
		commandsNumber.forEach(System.out::println);
		Assert.assertEquals(commandsNumber, arg1);
	}

	
	@Given("^client have at least one command in DB$")
	public void client_have_at_least_one_command_in_DB() throws Throwable {
	}

	@When("^I find a command of client by the identifier id \"(.*?)\" and the command idendifier id \"(.*?)\"$")
	public void i_find_a_command_of_client_by_the_identifier_id_and_the_command_idendifier_id(String arg1, String arg2) throws Throwable {
		HttpUriRequest request = new HttpGet("http://localhost:8080/rampup/api/"+arg1+"/commands/"+arg2);
		httpResponse = HttpClientBuilder.create().build().execute(request);
		command = RetrieveUtil.retrieveResourceFromResponse(httpResponse, CommandResource.class);

	}

	@Then("^I get the Identifier Command containing the name attribute value \"(.*?)\"$")
	public void i_get_the_Identifier_Command_containing_the_name_attribute_value(String arg1) throws Throwable {
		assertEquals(arg1,command.getName());
	}

	@Then("^number attribute value \"(.*?)\"$")
	public void number_attribute_value(String arg1) throws Throwable {
		assertEquals(Integer.parseInt(arg1),command.getNumber());
	}



}
