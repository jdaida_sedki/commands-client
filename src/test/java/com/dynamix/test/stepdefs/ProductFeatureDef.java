package com.dynamix.test.stepdefs;

import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertEquals;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.awaitility.Awaitility;

import com.dynamix.commands.dto.ProductResource;
import com.dynamix.test.ws.RetrieveUtil;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * 
 * @author fatma
 *
 */
public class ProductFeatureDef {

	HttpResponse httpResponse = null;

	ProductResource productModel;

	static {
		Awaitility.setDefaultTimeout(6000, TimeUnit.MILLISECONDS);
	}

	@Given("^the database have N products$")
	public void the_database_have_N_products() throws Throwable {
	}

	@When("^I Recover the Product with id \"(.*?)\"$")
	public void i_Recover_the_Product(String arg1) throws Throwable {
		HttpUriRequest request = new HttpGet("http://localhost:8080/rampup/api/products/" + arg1);
		httpResponse = HttpClientBuilder.create().build().execute(request);
		productModel = RetrieveUtil.retrieveResourceFromResponse(httpResponse, ProductResource.class);
		await().until(new Callable<Boolean>() {

			@Override

			public Boolean call() throws Exception {

				return productModel != null && productModel.getCode() != null;

			}

		}

		);
	}

	@Then("^I get the Identifier Product containing the code attribute value \"(.*?)\"$")
	public void i_get_the_Identifier_Product_containing_the_code_attribute_data(String arg1) throws Throwable {
		assertEquals(arg1, productModel.getCode());
	}

	@Then("^label attribute value \"(.*?)\"$")
	public void label_attribute_value(String arg1) throws Throwable {
		assertEquals(arg1, productModel.getLabel().toString());
	}

}
